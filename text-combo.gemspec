# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'text_combo/version'

Gem::Specification.new do |spec|
  spec.name          = "text_combo"
  spec.version       = TextCombo::VERSION
  spec.authors       = ["Jim Myhrberg"]
  spec.email         = ["contact@jimeh.me"]
  spec.description   = "quick hack thing just for a friend"
  spec.summary       = "quick hack thing just for a friend"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_runtime_dependency "sequel"
  spec.add_runtime_dependency "mysql2"
  spec.add_runtime_dependency "ruby-progressbar"
end
