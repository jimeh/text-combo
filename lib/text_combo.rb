require 'json'
require 'sequel'
require 'ruby-progressbar'

class TextCombo
  attr_accessor :operation
  attr_accessor :input_file
  attr_accessor :size
  attr_accessor :offset
  attr_accessor :limit
  attr_accessor :output_type
  attr_accessor :output_options

  def initialize(opts = {})
    @operation      = opts[:operation]      || "product"
    @input_file     = opts[:input_file]
    @size           = opts[:size]           || 2
    @offset         = opts[:offset]         || 0
    @limit          = opts[:limit]          || -1
    @output_type    = opts[:output_type]    || "print"
    @output_options = opts[:output_options] || {}
  end

  def output(ofst = nil, lim = nil)
    if output_type == "print"
      print_output(ofst, lim)
    elsif output_type == "mysql"
      mysql_output(ofst, lim)
    end
  end

  def print_output(ofst = nil, lim = nil)
    if output_options[:format] == "json"
      puts JSON.pretty_generate(combinations(ofst, lim))
    else
      puts combinations(ofst, lim).inspect
    end
  end

  def mysql_output(ofst = nil, lim = nil)
    uri        = @output_options[:uri]
    table_name = @output_options[:table_name].to_sym

    db = Sequel.connect(uri)
    bar = ProgressBar.create(
      :title  => "records",
      :format => "%t: [%b>>%i] (%c/%C) %E",
      :total  => index_combos.size,
      :throttle_rate => 0.1)

    combinations do |combo, count, total|
      bar.increment
      db[table_name].insert(nil, *combo)
    end
  end

  private

  def lines
    return [] if !File.exist?(input_file)
    @lines ||= File.readlines(input_file)
  end

  def indexes
    @indexes ||= (0..lines.size-1).to_a
  end

  def index_combos
    if operation == "product"
      @index_combos_product ||= indexes.product(indexes)
    elsif operation == "combination"
      @index_combos_combination ||= indexes.combination(size).to_a
    end
  end

  def combinations(ofst = nil, lim = nil, &block)
    ofst ||= offset
    lim  ||= limit

    min = ofst
    max = lim == -1 ? -1 : min + lim
    if block.nil?
      index_combos[min..max].map do |combo|
        combo.map { |index| lines[index] }
      end
    else
      total = lim == - 1 ? index_combos.size : lim
      index_combos[min..max].each_with_index do |combo, i|
        data = combo.map { |index| lines[index] }
        block.call(data, i+1, total)
      end
    end
  end
end
